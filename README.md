# AGV监控系统 #
### 背景 ###
传统的仓库拣选一般是采用人到货的方式，这种方式的优点在于建立流程的难度较低，尤其在人力资源成本较低的情况下，缺点有以下几个方面：

*  人的运行速度相对较慢，容易产生生物性疲劳，个人与个人之间的效率也不尽相同，造成拣选效率不稳定；
*  由于需要预留较大比例的交通通道，就意味着仓储密度较低。
*  人力成本总体成上升趋势，无法依靠简单地增加人力的方式应对业务快速增长的需求
*  对于特殊的环境恶劣的作业环境，人力甚至可能无法胜任

基于AGV的仓库拣选系统则拣选方式变成了货到人，这种方式的优点在于：

*  AGV可以保持稳定的速度和拣选效率，而且这个效率可以精确的计算出来，对于整个拣选系统的效率控制意义非常重要
*  AGV自动化程度高，大部分工作几乎不需要人工参与，在同样的人工成本下极大的提高拣选效率
*  提升业务能力高效方便，可以适应业务快速增长的场景
*  由于只需要提供AGV运行的路径，增大了仓库的使用率，降低了仓储成本

### AGV运行原理 ###
AGV运行首先需要精确定位AGV所在位置，一般通过地标上的位置标识码识别实现，AGV内的控制系统将自己的位置信息发送给计算中心，计算中心根据业务需要和AGV的位置分发给AGV合适的任务，AGV接收到任务后，将指定位置的货架搬运到拣选口进行拣选。

### AGV拣选的难点 ###
拣选系统的难点有5个方面：

* 系统与AGV之间是通过网络连接进行实时控制的，必须能够对可能出现的网络延迟和异常情况进行合理的处理，例如AGV突然出现异常无法运行，那就需要马上对该区域进行隔离，同时其它AGV的行走路径立即做出相应改变，保证整个AGV交通不会因此出现堵塞
* AGV路径的计算，首先需要保证最短路径算法提高效率，同时还需要考虑AGV的运行状态，AGV搬运货架时，通过通道行走，而不在搬运状态时，可以通过货架底部行走，增大通道的利用率
* 路径的计算方案的设计必须考虑到对AGV运行指挥可能出现的延迟和AGV的异常
* AGV密集运行时，路径交叉碰撞回避的设计，这方面有多种方案，需要根据实际情况选择，这关系到拣选的效率
* 系统的可扩展性，从30台到300台的支持，必须简单和高效

### 系统设计方案 ###
针对之上的原理，本系统设计了一系列的针对主要难点的解决方案

* 路径算法在基础的最短路径算法的基础上，加入标记障碍和适应路径的概念，AGV处于不同状态时，对路径的反应不同
* 摒弃常用的红绿灯控制算法，采用实时路径计算算法，保证AGV不会发生碰撞

### 系统功能 ###
系统提供以下功能：

* 仓库地图编辑器，为使用该系统的用户提供仓库设计基础数据提供接口，系统的计算基于该数据
* AGV状态实时监控接口和页面
* AGV路径计算
* 系统对大量AGV的支持（TODO）