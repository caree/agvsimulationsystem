package controllers

import (
	// "encoding/json"
	"fmt"
	"github.com/astaxie/beego"
	// "github.com/bitly/go-simplejson"
	// "strconv"
	"runtime"
	"strings"
)

/*
	开发系统的核心思想是通过一个模型对数据依据一定的逻辑规则进行处理，模型应该在相应环节发出系统运行提示，可以分为4级
	1  与设计不符合的部分，未预测到会出现的异常，遇见必须马上处理
	2  流程关键运转关键点和提示信息，可以预料的异常
	3  模型内部运转关键点
	4  调试环节关键节点的提示
	相应可以添加更低级别的提示，以适应调试的需要
*/

func init() {
	beego.BeeLogger.EnableFuncCallDepth(false)
	beego.BeeLogger.SetLogFuncCallDepth(5)

}

var DebugLevel int = 4

var userBeego = true

func DebugOutput(log string, level int) {
	if level <= DebugLevel {
		if userBeego == true {
			DebugOutputBeego(log, level)
		} else {

			prefix := ""
			switch level {
			case 1:
				prefix = "******"
			case 2:
				prefix = "------"
			case 3:
				prefix = "      "
			case 4:
				prefix = "             "
			}
			fmt.Println(prefix + log)
		}
	}
}

func GetFileLocation() string {
	_, file, line, ok := runtime.Caller(1)
	if ok {
		array := strings.Split(file, "/")
		return fmt.Sprintf(" (%s %d)", array[len(array)-1], line)
	} else {
		return "  ???"
	}
}

func DebugOutputStrings(strs []string, level int) {
	for _, str := range strs {
		DebugOutput(str, level)
	}
}

func DebugOutputBeego(log string, level int) {
	switch level {
	case 1:
		beego.Error(log)
	case 2:
		beego.Notice(log)
	case 3:
		beego.Informational(log)
	case 4:
		beego.Debug(log)
	}
}
