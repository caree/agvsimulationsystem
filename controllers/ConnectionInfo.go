package controllers

import (
	"fmt"
	"net"
)

type ConnectionInfo struct {
	IP   string
	Port int
	Conn *(net.Conn)
}

func (this ConnectionInfo) toString() string {
	return fmt.Sprintf("ConnectionInfo: %s:%d", this.IP, this.Port)
}

type ConnectionInfoList []ConnectionInfo

func (this *ConnectionInfoList) removeClientFromList(host string, port int) {
	for i, connInfo := range *this {
		if connInfo.IP == host && connInfo.Port == port {
			if i == 0 {
				*this = (*this)[1:]
			} else {
				*this = append((*this)[0:i], (*this)[i+1:]...)
			}
		}
	}
}
func (this *ConnectionInfoList) addClientToList(host string, port int, conn *(net.Conn)) {
	(*this) = append(*this, ConnectionInfo{
		IP:   host,
		Port: port,
		Conn: conn,
	})
}
