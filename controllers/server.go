package controllers

import (
	"agvSimulationSystem/simulator"
	// "encoding/json"
	"fmt"
	// "github.com/astaxie/beego"
	"net"
	"time"
	// "os"
	"regexp"
	// "runtime"
	// "strconv"
	"strings"
)

const maxRead = 1024

var (
	g_TCPServer    *net.TCPListener
	msgTemp        string             = ""
	validMsg                          = regexp.MustCompile(`\[[\w,\{\}\(\)\*\s]+\]`)
	simulators                        = simulator.AGVSimulatorCoreList{}
	connectionList ConnectionInfoList = ConnectionInfoList{}
)

func init() {
	simulators = append(simulators, simulator.NewAGVSimulatorCore("01", "10", "19", simulator.AGV_STATUS_RUNNING))
	simulators = append(simulators, simulator.NewAGVSimulatorCore("02", "1", "2", simulator.AGV_STATUS_RUNNING))
	simulators.StartRunning(2 * time.Second)

	funcReceiveAgvData := func(data *simulator.AgvData) {
		msg := data.ToCommandString()
		speak2clients(msg) //将模拟器的数据发送给调度系统
	}
	simulators.RegisteAGVStatusDataReceiver(funcReceiveAgvData)

	startServer()
}

func startServer() {
	host := "127.0.0.1"
	port := 9001
	hostAndPort := fmt.Sprintf("%s:%d", host, port)
	g_TCPServer = createListener(hostAndPort)
	if g_TCPServer == nil {
		return
	}
	for {
		conn, err := g_TCPServer.Accept()
		if err != nil {
			DebugOutput(err.Error()+GetFileLocation(), 1)
			break
		}
		// checkError(err, "Accept: ")
		go onNewConnection(conn, host, port)
	}
}

func createListener(hostAndPort string) *net.TCPListener {
	serverAddr, err := net.ResolveTCPAddr("tcp", hostAndPort)
	if err != nil {
		DebugOutput(err.Error()+GetFileLocation(), 1)
		return nil
	}
	listener, err := net.ListenTCP("tcp", serverAddr)
	if err != nil {
		DebugOutput(err.Error()+GetFileLocation(), 1)
		return nil
	}
	DebugOutput(fmt.Sprintf("开始监听: %s", listener.Addr().String())+GetFileLocation(), 2)
	return listener
}
func onNewConnection(conn net.Conn, host string, port int) {
	connFrom := conn.RemoteAddr().String()
	DebugOutput(fmt.Sprintf("与 %s 建立连接", connFrom), 2)
	connectionList.addClientToList(host, port, &conn)
	// sayHello(conn)
	defer func() {
		err := conn.Close()
		connectionList.removeClientFromList(host, port)
		DebugOutput(fmt.Sprintf("关闭客户端连接: %s", connFrom)+GetFileLocation(), 2)
		if err != nil {
			DebugOutput(err.Error()+GetFileLocation(), 1)
		}
	}()
	for {
		var ibuf []byte = make([]byte, maxRead)
		if length, err := conn.Read(ibuf[0:maxRead]); err == nil {
			// ibuf[maxRead] = 0 // to prevent overflow
			// handleMsg(length, err, ibuf)
			if length > 0 {
				msg := string(ibuf)
				DebugOutput("接收到："+msg+GetFileLocation(), 4)
				msgTemp = splitMsgToCmd(msgTemp + (msg))
			}
		} else {
			return
		}
	}
}
func speak2clients(msg string) {
	for _, connInfo := range connectionList {
		if _, err := (*(connInfo.Conn)).Write([]byte(msg)); err != nil {
			// checkError(err, "Write: wrote "+string(wrote)+" bytes.")
			DebugOutput(err.Error()+GetFileLocation(), 2)
		} else {
			DebugOutput("发出数据："+msg+GetFileLocation(), 3)
		}
	}
}

/*func sayHello(to net.Conn) {
	obuf := []byte("hello, I am the AGV server!")
	_, err := to.Write(obuf)
	if err != nil {
		DebugOutput(err.Error()+GetFileLocation(), 2)
	}
}
*/
func splitMsgToCmd(rawData string) string {
	// DebugOutput(rawData+GetFileLocation(), 1)
	rest := ""
	if i := strings.LastIndex(rawData, "]"); i >= 0 {
		msges := rawData[0 : i+1]
		// DebugOutput("要解析的命令串："+msges, 2)
		rest = rawData[i+1:]
		for _, msg := range validMsg.FindAllString(msges, -1) {
			cmd := strings.Split(msg[1:len(msg)-1], ",")
			task, agvID := parseCMD(cmd)
			if task == nil {
				DebugOutput("命令解析部分出现错误"+GetFileLocation(), 2)
			} else {
				DebugOutput(fmt.Sprintf("解析命令结果：AGV %s 执行命令 %s", agvID, task.String())+GetFileLocation(), 2)
				simulators.ReceiveTaskCommand(agvID, task)
			}
		}
	}
	return rest
}

// 解析命令
// 命令格式类似于： [1,01,1,{(1 1 1001)*(1 2 1002)*(1 3 1003)}]
// 返回命令内容和要执行任务的ID
func parseCMD(values []string) (*simulator.TaskCommand, string) {
	if len(values) < 4 {
		return nil, ""
	}
	guid := values[0]
	taskType := values[1]
	agvID := values[2]
	liftStatus := values[3]
	path := values[4]
	strPoints := strings.Split(strings.Trim(path, "{}"), "*")
	list := simulator.PathPointList{}
	for _, strPoint := range strPoints {
		// fmt.Println(strPoint)
		if len(strPoint) <= 0 {
			continue
		}
		pointValues := strings.Split(strings.Trim(strPoint, "()"), " ")
		list = append(list, simulator.NewPathPoint(pointValues[0], pointValues[1], pointValues[2]))
	}

	taskCommand := simulator.TaskCommand{
		GUID:       guid,
		TaskType:   taskType,
		Path:       list,
		LiftStatus: liftStatus,
	}
	return &taskCommand, agvID
}
