package simulator

import (
	"fmt"
	// "strings"
	// "strconv"
	// "time"
)

//发送给AGVSoul的具体运行指令，功能在于指示AGV具体如何完成任务，分为两种类型：行走和举升，AGVSoul需要根据类型提取参数
// AGVTaskItem在于描述AGV完成什么任务
type TaskCommand struct {
	TaskType   string
	Path       PathPointList
	LiftStatus string
	GUID       string
}

func (this *TaskCommand) ToCommandString(agvID string) string {
	return fmt.Sprintf("[%s,%s,%s,%s,%s]", this.GUID, this.TaskType, agvID, this.LiftStatus, this.Path.FormatPathToTransfer())
}

func (this *TaskCommand) IsTheSame(task *TaskCommand) bool {
	return this.GUID == task.GUID
}
func (this *TaskCommand) String() string {
	str := "代号：" + this.GUID
	switch this.TaskType {
	case TASK_TYPE_MOVE_TO:
		return str + " " + fmt.Sprintf("移动 路径是：%s", this.Path.FormatPath())
	case TASK_TYPE_LIFT:
		switch this.LiftStatus {
		case AGV_LIFT_STATUS_UNLIFTED:
			return str + " " + "收起举升"
		case AGV_LIFT_STATUS_LIFTED:
			return str + " " + "进行举升"
		}
	}
	return str + " " + "任务无法描述"
}
