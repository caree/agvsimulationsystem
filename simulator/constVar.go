package simulator

import (
// "fmt"
)

//AGV任务类型
const (
	TASK_TYPE_MOVE_TO string = "1"
	TASK_TYPE_LIFT    string = "2"
)

//AGV的举升状态
const (
	AGV_LIFT_STATUS_UNLIFTED string = "0"
	AGV_LIFT_STATUS_LIFTED   string = "1"
)

//AGV的运行状态
const (
	AGV_STATUS_UNCONNECTED     string = "0"
	AGV_STATUS_RUNNING         string = "1"
	AGV_STATUS_RUNNING_PAUSED  string = "2"
	AGV_STATUS_RUNNING_STOPPED string = "3"
)

//接收到的agv信息类型
const (
	ACCEPTED_CMD_TYPE_HEART_BEATING           string = "1" //心跳
	ACCEPTED_CMD_TYPE_AGV_POSITION_CHANGED    string = "2" //位置变化
	ACCEPTED_CMD_TYPE_AGV_STATUS_CHANGED      string = "3" //agv状态变化
	ACCEPTED_CMD_TYPE_AGV_LIFT_STATUS_CHANGED string = "4" //举升状态变化
)

//发送给agv的控制命令
const (
	CMD_MOVE_AGV       = "31"
	CMD_SET_AGV_STATUS = "32"
	CMD_SET_NEXT_STEP  = "33"
	CMD_SET_AGV_LIFT   = "34"
)

/*
const (
	EVENT_JOIN             = 0 //0
	EVENT_AGV_HEARTBEATING = 1
	EVENT_POSITION_NEW     = 2
	EVENT_STATUS_NEW       = 3
	EVENT_LIFT_STATUS_NEW  = 4

	maxRead = 1024
)
*/
