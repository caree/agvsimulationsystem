package simulator

import (
	// "agvViewer/
	// _ "agvViewer/routers"
	// "path/filepath"
	// "runtime"
	"fmt"
	"testing"
	// "time"
	// "github.com/astaxie/beego"
	// . "github.com/smartystreets/goconvey/convey"
	// "strconv"
	"strings"
)

func init() {
	// fmt.Println("TestPathFinding init ...")
}

func TestAGVPathFinding(t *testing.T) {

	// tryTask001_2(t)
	// tryTask001_1(t)
	// tryTask001(t)
}
func tryTask001_2(t *testing.T) {
	fmt.Println("测试 001_2")
	fmt.Println("### 测试 TaskCommand 序列和和解析 举升状态为空...")
	path := PathPointList{}
	taskCommand := TaskCommand{
		TASK_TYPE_MOVE_TO, path, "", "1004",
	}
	str := taskCommand.ToCommandString("01")
	fmt.Println(str)
	newCommand, agvID := parseCMD(strings.Split(str[1:len(str)-1], ","))
	if newCommand == nil ||
		newCommand.GUID != "1004" ||
		newCommand.TaskType != TASK_TYPE_MOVE_TO ||
		path.CompareTo(newCommand.Path) == false ||
		agvID != "01" {
		t.FailNow()
	} else {
		fmt.Println(newCommand.String())
	}

	fmt.Println("### 001_2 测试完成 ...")
}
func tryTask001_1(t *testing.T) {
	fmt.Println("测试 001_1")
	fmt.Println("### 测试 TaskCommand 序列和和解析 路径为空...")
	path := PathPointList{}
	taskCommand := TaskCommand{
		TASK_TYPE_MOVE_TO, path, AGV_LIFT_STATUS_LIFTED, "1004",
	}
	str := taskCommand.ToCommandString("01")
	fmt.Println(str)
	newCommand, agvID := parseCMD(strings.Split(str[1:len(str)-1], ","))
	if newCommand == nil ||
		newCommand.GUID != "1004" ||
		newCommand.TaskType != TASK_TYPE_MOVE_TO ||
		newCommand.LiftStatus != AGV_LIFT_STATUS_LIFTED ||
		path.CompareTo(newCommand.Path) == false ||
		agvID != "01" {
		t.FailNow()
	}

	fmt.Println("### 001_1 测试完成 ...")
}
func tryTask001(t *testing.T) {
	fmt.Println("测试 001")
	fmt.Println("### 测试 TaskCommand 序列和和解析 ...")
	path := PathPointList{
		&PathPoint{"1", "1", "1001"},
		&PathPoint{"1", "2", "1002"},
		&PathPoint{"1", "3", "1003"},
	}
	taskCommand := TaskCommand{
		TASK_TYPE_MOVE_TO, path, AGV_LIFT_STATUS_LIFTED, "1004",
	}
	str := taskCommand.ToCommandString("01")
	fmt.Println(str)
	newCommand, agvID := parseCMD(strings.Split(str[1:len(str)-1], ","))
	if newCommand == nil ||
		newCommand.GUID != "1004" ||
		newCommand.TaskType != TASK_TYPE_MOVE_TO ||
		newCommand.LiftStatus != AGV_LIFT_STATUS_LIFTED ||
		path.CompareTo(newCommand.Path) == false ||
		agvID != "01" {
		t.FailNow()
	}

	fmt.Println("### 001 测试完成 ...")
}

// 解析命令
// 命令格式类似于： [1,01,1,{(1 1 1001)*(1 2 1002)*(1 3 1003)}]
// 返回命令内容和要执行任务的ID
func parseCMD(values []string) (*TaskCommand, string) {
	if len(values) < 4 {
		return nil, ""
	}
	guid := values[0]
	taskType := values[1]
	agvID := values[2]
	liftStatus := values[3]
	path := values[4]
	strPoints := strings.Split(strings.Trim(path, "{}"), "*")
	list := PathPointList{}
	for _, strPoint := range strPoints {
		fmt.Println(strPoint)
		if len(strPoint) <= 0 {
			continue
		}
		pointValues := strings.Split(strings.Trim(strPoint, "()"), " ")
		list = append(list, NewPathPoint(pointValues[0], pointValues[1], pointValues[2]))
	}

	taskCommand := TaskCommand{
		GUID:       guid,
		TaskType:   taskType,
		Path:       list,
		LiftStatus: liftStatus,
	}
	return &taskCommand, agvID
}
