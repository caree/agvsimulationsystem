package simulator

import (
	"fmt"
	// "strings"
	"time"
)

/*
	AGVSimulatorCore 模拟了一个AGV基本的行为

	- 接收行驶路径
	- 按照指定路径行驶，每个路径点带有GUID，下一步的点的GUID一定比现在的大
	- 可以设定速度


*/
var (
	HEART_BEAT_INTERVAL time.Duration = 2 * time.Second
)

type AgvDataReceiver func(data *AgvData)

type AGVSimulatorCore struct {
	ID                string
	CurrentPoint      *PathPoint //当前的位置
	LiftStatus        string
	Status            string
	TaskCommand       *TaskCommand
	AgvDataSender     AgvDataReceiver
	ChIn_TaskReceiver chan *TaskCommand //接收执行的动作
	ChTicker          <-chan time.Time  //模拟运行速度
	ChHeartbreak      <-chan time.Time  //模拟心跳
}

func NewAGVSimulatorCore(id, x, y, status string) *AGVSimulatorCore {
	simulator := &AGVSimulatorCore{
		ID:         id,
		LiftStatus: AGV_LIFT_STATUS_UNLIFTED,
		Status:     status,
		CurrentPoint: &PathPoint{
			X:    x,
			Y:    y,
			GUID: "0",
		},
		ChIn_TaskReceiver: make(chan *TaskCommand, 10),
	}
	return simulator
}
func (this *AGVSimulatorCore) RegisteAGVStatusDataReceiver(receiver AgvDataReceiver) {
	this.AgvDataSender = receiver
}
func (this *AGVSimulatorCore) StartRunning(v time.Duration) {
	DebugOutput(fmt.Sprintf("AGV模拟器(%s) 开始运行", this.ID)+GetFileLocation(), 2)
	this.ChTicker = time.Tick(v)
	this.ChHeartbreak = time.Tick(HEART_BEAT_INTERVAL)
	go func() {
		for {
			select {
			case <-this.ChTicker:
				// DebugOutput("AGV模拟器进行下一步动作..."+GetFileLocation(), 3)
				this.NextStep()
			case cmd := <-this.ChIn_TaskReceiver:
				// DebugOutput(fmt.Sprintf("模拟器AGV( %s ) 接收到命令：%s", this.ID, cmd.String())+GetFileLocation(), 3)
				this.ReceiveCommand(cmd)

			case <-this.ChHeartbreak:
				// DebugOutput("AGV模拟器心跳..."+GetFileLocation(), 3)
				this.HeartBreak()
			}
		}
	}()
}

func (this *AGVSimulatorCore) HeartBreak() {
	if this.AgvDataSender != nil {
		agvData := &AgvData{
			ID:         this.ID,
			DataType:   ACCEPTED_CMD_TYPE_HEART_BEATING,
			X:          this.CurrentPoint.X,
			Y:          this.CurrentPoint.Y,
			LiftStatus: this.LiftStatus,
			Status:     this.Status,
		}
		// this.ChOut_DataReceiver <- agvData
		this.AgvDataSender(agvData)
	} else {
		DebugOutput("AGV状态无法输出"+GetFileLocation(), 2)
	}
}
func (this *AGVSimulatorCore) ReceiveCommand(command *TaskCommand) {
	if this.TaskCommand == nil || this.TaskCommand.IsTheSame(command) == false {
		DebugOutput(fmt.Sprintf("模拟器AGV %s 设置新命令：%s", this.ID, command.String())+GetFileLocation(), 2)
		this.TaskCommand = command
	}
}

func (this *AGVSimulatorCore) UpdateLiftStatus(liftStatus string) bool {
	if this.LiftStatus == liftStatus {
		return false
	}
	this.LiftStatus = liftStatus
	if this.LiftStatus == AGV_LIFT_STATUS_LIFTED {
		DebugOutput("AGV模拟器进行了 举升"+GetFileLocation(), 3)
	} else if this.LiftStatus == AGV_LIFT_STATUS_UNLIFTED {
		DebugOutput("AGV模拟器进行了 降下举升"+GetFileLocation(), 3)
	}
	return true
}
func (this *AGVSimulatorCore) UpdatePosition(point *PathPoint) bool {

	//如果要行走的下一步就是当前所在的点，就等于没走
	if this.CurrentPoint.X == point.X && this.CurrentPoint.Y == point.Y {
		if this.CurrentPoint.GUID <= "0" { //只是初始化完
			this.CurrentPoint.GUID = point.GUID
		}
		return false
	}
	if point.GUID <= this.CurrentPoint.GUID { //guid小表明之前已经走过该点
		return false
	}
	this.CurrentPoint.X = point.X
	this.CurrentPoint.Y = point.Y
	this.CurrentPoint.GUID = point.GUID
	DebugOutput(fmt.Sprintf("AGV模拟器(%s) 行走至 (%s, %s)", this.ID, this.CurrentPoint.X, this.CurrentPoint.Y), 3)
	return true
}
func (this *AGVSimulatorCore) NextStep() {
	if this.TaskCommand == nil {
		return
	}
	switch this.TaskCommand.TaskType {
	case TASK_TYPE_LIFT:
		if this.UpdateLiftStatus(this.TaskCommand.LiftStatus) == false {
			return
		} else {
			agvData := &AgvData{
				ID:         this.ID,
				DataType:   ACCEPTED_CMD_TYPE_AGV_LIFT_STATUS_CHANGED,
				X:          this.CurrentPoint.X,
				Y:          this.CurrentPoint.Y,
				LiftStatus: this.LiftStatus,
				Status:     this.Status,
			}
			this.AgvDataSender(agvData)
		}
	case TASK_TYPE_MOVE_TO:
		path := this.TaskCommand.Path
		if len(path) > 0 {
			point := path[0]
			path = path[1:]
			this.TaskCommand.Path = path
			if this.UpdatePosition(point) == false {
				this.NextStep()
				return
			}
			if this.AgvDataSender != nil {
				agvData := &AgvData{
					ID:         this.ID,
					DataType:   ACCEPTED_CMD_TYPE_AGV_POSITION_CHANGED,
					X:          this.CurrentPoint.X,
					Y:          this.CurrentPoint.Y,
					LiftStatus: this.LiftStatus,
					Status:     this.Status,
				}
				this.AgvDataSender(agvData)
			} else {
				DebugOutput("AGV状态无法输出"+GetFileLocation(), 2)
			}
		}
	}
}

//-------------------------------------------------------------------

type AGVSimulatorCoreList []*AGVSimulatorCore

func (this AGVSimulatorCoreList) RegisteAGVStatusDataReceiver(receiver AgvDataReceiver) {
	for _, simulator := range this {
		simulator.RegisteAGVStatusDataReceiver(receiver)
	}
}
func (this AGVSimulatorCoreList) ReceiveTaskCommand(agvID string, task *TaskCommand) {
	simulator := this.FindAGVSimulatorByID(agvID)
	if simulator == nil {
		DebugOutput("不存在模拟器 "+agvID+GetFileLocation(), 2)
	} else {
		simulator.ChIn_TaskReceiver <- task
	}
}
func (this AGVSimulatorCoreList) StartRunning(v time.Duration) {
	for _, simulator := range this {
		simulator.StartRunning(v)
	}
}
func (this AGVSimulatorCoreList) FindAGVSimulatorByID(id string) *AGVSimulatorCore {
	for _, simulator := range this {
		if simulator.ID == id {
			return simulator
		}
	}
	return nil
}
