package main

import (
	_ "agvSimulationSystem/controllers"
	_ "agvSimulationSystem/docs"
	_ "agvSimulationSystem/routers"

	"github.com/astaxie/beego"
)

func main() {
	// if beego.RunMode == "dev" {
	// 	beego.DirectoryIndex = true
	// 	beego.StaticDir["/swagger"] = "swagger"
	// }
	beego.Run()
}
